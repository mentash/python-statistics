'''
The 4 aspects associated with describing quantitative variables
They are not used to describe categorical variables.
1. Measures of Center
2. Measures of Spread
3. Shape of the Distribution
4. Outliers

Analysing both discrete and continuous quantitative data
1. Center
    1. Mean
    2. Median
    3. Modes

2. Spread (5 Number Summary)
    1. Maximum
    2. Third Quartile
    3. Second Quartile (Median)
    4. First Quartile
    5. Minimum
Measuring Spread
    6. Range
    7. Interquartile Range
    8. Standard Deviation
    9. Variance



3. Shape of the Distribution
    1. Right-skewed
    2. Left-skewed
    3. Symmetric (frequently normally distributed)


Note:
    When we have data that follows a normal distribution,
    we can completely understand our dataset using the mean and standard deviation.

    However, if our dataset is skewed,
    the 5 number summary (and measures of center associated with it) might be better to summarize our dataset.

4. Outliers
'''

import statistics as stat
import collections as col
import scipy as sp
from scipy import stats
import numpy as np


# Returns a list of integers
def get_data():
    # Ask user for data
    print('Paste you data below or hit enter to input manually')
    data_list = (input('Enter comma separated values: '))
    if len(data_list) > 1:

        # Convert string to a list of strings
        data_list = [int(s) for s in data_list.split(',')]
        print(data_list)
    # Enter data manually
    if len(data_list) < 1:
        print('Enter numbers manually and hit enter')
        nums = []
        while True:
            x = input('Please enter a number: ')
            nums.append(x)
            if len(x) < 1:
                print('Preparing your data report...')
                print('---------------------------------')
                nums.pop()
                break
            data_list = list(map(int, nums))
    else:
        data_list = list(map(int, data_list))

    return data_list

def sort_data(data):
    sorted_data = sorted(data)
    return sorted_data

dataset = get_data()
sorted_dataset = sort_data(dataset)

def handle_mode(d):
    # Check for no common values
    d = sorted_dataset
    count = col.Counter(d)
    for value in count.most_common():
        if value[1] == 1:
            # Single unique common values
            print('no common value')
        else:
            try:
                dmode = stat.mode(d)
                return dmode
            # Two equally common values
            except stat.StatisticsError:
                print('no unique mode or found 2 equally common values')
                # test for count lst elements
                lst_count = [[x, d.count(x)] for x in set(d)]
                #print(lst_count)

                # remove count <= 1
                lst_count = [x for x in set(d) if d.count(x) > 1]
                #print(lst_count)

                # get common values by index
                #print('mode is either: ', lst_count[0], 'or', lst_count[1])
                return lst_count[0], lst_count[1]
            #print('count', count)
            #print('count', count.most_common())
        return 'no mode'

'''
15, 4, 3, 8, 15, 22, 7, 9, 2, 3, 3, 12, 6

n = 13
Median = 7
First quartile = 3
Third quartile = 13.5
Mean  = 8.4
Mode = 3

Inter Quartile Range = 10.5
range = 20
Variance = 33.9
Standard Deviation = 5.8
Minimum = 2
Maximum = 22

'''

# create increasing order array
a = sorted(np.array(get_data()))

mode = sp.stats.mode(a)
print(mode)

mean = stat.mean(dataset)
median = stat.median(dataset)
mode = handle_mode(sorted_dataset)

maximum = max(sorted_dataset)
minimum = min(sorted_dataset)
variance = stat.variance(sorted_dataset)
st_deviation = stat.stdev(sorted_dataset)

print('Your data set is: ', dataset)
print('Your sorted data set is: ', sorted_dataset)
print('First. Measures of Center ----------------')
print('1. Mean is: ', mean)
print('2. Median is: ', median)
print('3. Mode is: ', mode)
print('---------------------------------------------')
print('Second. Measure of spread')