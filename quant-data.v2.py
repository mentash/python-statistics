'''
Analysing both discrete and continuous quantitative data
1. Center
    1. Mean
    2. Median
    3. Mode

2. Spread (5 Number Summary)
    1. Maximum
    2. Third Quartile
    3. Second Quartile (Median)
    4. First Quartile
    5. Minimum

3. Shape
4. Outliers
'''

import statistics as stat
import collections as col

# Returns a list of integers
def get_data():
    # Ask user for data
    print('Paste you data below or hit enter to input manually')
    data_list = (input('Enter comma separated values: '))
    if len(data_list) > 1:

        # Convert string to a list of strings
        data_list = [int(s) for s in data_list.split(',')]
        print(data_list)
    # Enter data manually
    if len(data_list) < 1:
        print('Enter numbers manually and hit enter')
        nums = []
        while True:
            x = input('Please enter a number: ')
            nums.append(x)
            if len(x) < 1:
                print('Preparing your data report...')
                print('---------------------------------')
                nums.pop()
                break
            data_list = list(map(int, nums))
    else:
        data_list = list(map(int, data_list))

    return data_list


# Sort data in ascending order
def sort_data(data):
    sorted_data = sorted(data)
    return sorted_data


dataset = get_data()
sorted_dataset = sort_data(dataset)


def handle_mode(d):
    # Check for no common values
    d = sorted_dataset
    count = col.Counter(d)
    for value in count.most_common():
        if value[1] == 1:
            # Single unique common values
            print('no common value')
        else:
            try:
                dmode = stat.mode(d)
                return dmode
            # Two equally common values
            except stat.StatisticsError:
                print('no unique mode or found 2 equally common values')
                # test for count lst elements
                lst_count = [[x, d.count(x)] for x in set(d)]
                #print(lst_count)

                # remove count <= 1
                lst_count = [x for x in set(d) if d.count(x) > 1]
                #print(lst_count)

                # get common values by index
                #print('mode is either: ', lst_count[0], 'or', lst_count[1])
                return lst_count[0], lst_count[1]
            #print('count', count)
            #print('count', count.most_common())
        return 'no mode'



mean = stat.mean(dataset)
median = stat.median(dataset)
mode = handle_mode(sorted_dataset)

maximum = max(sorted_dataset)
minimum = min(sorted_dataset)
variance = stat.variance(sorted_dataset)
st_deviation = stat.stdev(sorted_dataset)

print('Your data set is: ', dataset)
print('Your sorted data set is: ', sorted_dataset)
print('First. Measures of Center ----------------')
print('1. Mean is: ', mean)
print('2. Median is: ', median)
print('3. Mode is: ', mode)
print('---------------------------------------------')
print('Second. Measure of spread')

print('variance', variance)



