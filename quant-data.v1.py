"""
The main purpose of this module is to practice some statistical terms
used to evaluate quantitative data, by creating algorithmic functions
based of the formulas of those terms
Otherwise, python is loaded with tons of libraries that can do that easily
"""

dataset = (input("please enter comma separated numbers ")).split(",")


# convert str numbers to ints
dataset = list(map(int, dataset))
sorted_set = sorted(dataset)
print(dataset)


# find mean
def mean(dataset):
    return sum(dataset)/len(dataset)


# find distance of each data from mean
def spread(dataset):
    dist = [x-mean(dataset) for x in dataset]
    return dist


# find variance
def sqr_dist(dataset):
    sqr_lst = [(x-mean(dataset))**2 for x in dataset]

    #print(sqr_lst)
    return sum(sqr_lst)


#variance
def variance(dataset):
    return sqr_dist(dataset)/len(dataset)

variance1 = variance(dataset)
variance2 = stat.variance(dataset)

print("variance 1: ", variance1)
print("variance 2: ", variance2)

# standard deviation
def std_dev(dataset):
    return variance(dataset)**(1/2)

print("data set = ", dataset)
print("sum of values = ", sum(dataset))
print("mean = ", mean(dataset))
print("distance from mean = ", spread(dataset))
print("square distance from mean = ", sqr_dist(dataset))
print("variance = ", round(variance(dataset), 2))
print("standard deviation = ", round(std_dev(dataset), 2))